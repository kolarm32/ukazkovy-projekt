import * as React from "react";
import Header from "./components/IndexPage/Header";
import Body from "./components/IndexPage/Body";
import Profile from "./components/main/profile/profile";
import main from "./components/main/main";
import {Route} from "react-router-dom";

export default class Routes extends React.Component {

    render() {
        return (
                <div>
                    {<Route exact path="/" component={Header}/>}
                    {<Route exact path="/" component={Body}/>}
                    {/*{<Route path="/test" component={test}/>}*/}
                    {<Route path="/profile" component={Profile}/>}
                    {<Route path="/main" component={main}/>}
                </div>
        );
    }
}