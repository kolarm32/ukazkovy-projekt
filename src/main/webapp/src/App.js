import React from 'react';
import './App.css';
import './styles/styles.css';
import Header from "./components/IndexPage/Header";
import {BrowserRouter, Route} from 'react-router-dom'
import ContextUser from "./components/appContext";
import Profile from "./components/main/profile/profile";
import UserProfile from "./components/main/profile/userProfile";
import Cookies from "js-cookie";
import {Switch} from "react-bootstrap";
import Newsfeed from "./components/main/newsfeed";
import Messages from "./components/main/messages/messages";
import IndexHeader from "./components/IndexPage/Header";
import IndexBody from "./components/IndexPage/Body";
import groupChat from "./components/main/groupChat";
import Search from "./components/main/search";

class App extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            logged: null,
            authenticated: false,
            loading: true,
        }
    }

    componentDidMount = async () => {
        if (Cookies.get("JSESSIONID") && this.state.user == null && this.state.authenticated === false) {
            await this.tryLogin();
            console.log("after try");
        }
    };

    // callback for updating auth
    updateAuth = (bool) => {
        this.setState({authenticated: bool})
    }

    // callback for updated logged user
    updateLogged = (user) => {
        this.setState({logged: user})
    }

    logout = () => {
        Cookies.remove("JSESSIONID");
        this.setState({authenticated: false});
        this.setState({logged: null});
    };

    async tryLogin() {
        fetch("https://my-social-network-x.herokuapp.com/user/current", {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
            },
            credentials: "include",
        })
            .then((response) => {
                if (response.ok) {
                    return response.json();
                } else {
                    console.error("NEPODARILO SE prihlasit" + response.statusText + "   " + response.status)
                    this.logout();
                }
            })
            .then((data) => {
                this.updateLogged(data);
                this.updateAuth(true);
            }).catch(() => {
                this.logout();
        });
    }

    render() {
        const {logged, authenticated, uid, loading} = this.state;

        return (
            <ContextUser.Provider value={{
                state: {logged, authenticated, uid, loading},
                actions: {
                    updateLogged: this.updateLogged,
                    updateAuth: this.updateAuth,
                    logout: this.logout,
                    tryLogin: this.tryLogin
                }
            }}>

                <BrowserRouter>
                    {this.state.authenticated === true ?
                        <div>
                            <Route component={Header}/>
                            <Switch>
                                <Route exact={true} path="/" component={Newsfeed}/>
                                <Route path="/profile" component={Profile}/>
                                <Route path="/messages" component={Messages}/>
                                <Route path="/groupChat" component={groupChat}/>
                                <Route path="/search" component={Search}/>
                                <Route path="/viewProfile" component={UserProfile}/>
                            </Switch>
                        </div>
                        :
                        <div>
                        <Route exact={true} path="/" component={IndexHeader}/>
                        <Route exact={true} path="/" component={IndexBody}/>
                        </div>

                    }
                </BrowserRouter>
            </ContextUser.Provider>
        );
    }
}

export default App;