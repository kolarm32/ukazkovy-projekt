import React from 'react';
import "./newsfeed.css"
import postForm from "./postForm";
import {Route} from "react-router-dom";
import {Form} from "react-bootstrap";
import ContextUser from "../appContext";

export default class Newsfeed extends React.Component {
    static contextType = ContextUser;

    constructor(props) {
        super(props);

        this.state = {
            user: {},
            posts: null,
            isLoading: true,
            interval: null
        }
    }

    componentDidMount() {
        this.setState({user: this.context.state.logged})
        this.setState({interval: setInterval((() => this.getPosts()), 1000)})
    }

    getPosts = async () => {
        fetch('https://my-social-network-x.herokuapp.com/posts')
            .then(response=> response.json())
            .then(data=> this.setState({posts: data,isLoading: false}))
    }

    componentWillUnmount() {
        clearInterval(this.interval);
        clearInterval(this.state.interval);
    }

    render(){
        if(this.state.isLoading){
            return <h3>Just a moment, loading...</h3>;
        }
        return (
            <main>
                <Route component={postForm}/>
                <div className="posts">
                    <h4>Newsfeed</h4>
                    {this.state.posts.map(post =>
                        <Form key={post.id} className={"windows radius post"}>
                            <Form.Label>{post.owner.firstName + " " + post.owner.lastName + " " + post.timestamp.substr(0, 10)}</Form.Label>
                            <Form.Group>
                                <Form.Control as={"textarea"} readOnly={true}
                                              value={post.postContent} disabled={true}/>
                            </Form.Group>
                        </Form>
                    )}
                </div>
        </main>);
    }
}