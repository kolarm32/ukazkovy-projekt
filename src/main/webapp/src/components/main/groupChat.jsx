import React from "react";
import SockJsClient from "react-stomp";
import {Launcher} from 'react-chat-window'
import { TalkBox } from "react-talk";

export default class groupChat extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            clientConnected: false,
            messages: ["x","cascsa","ascasc"]
        };

    }

    componentDidMount() {
        fetch("https://my-social-network-x.herokuapp.com/history", {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
            },
            credentials: "include",
        }).then((response) => {
            //TODO : dodelat historii groupchatu

            // console.log(response.body)
        }).then((data)=>{
            // console.log(data.json())
        });
    }

    onMessageReceive = (msg, topic) => {
        this.setState(prevState => ({
            messages: [...prevState.messages, msg]
        }));
    }

    sendMessage = (msg, selfMsg) => {
        try {
            this.clientRef.sendMessage("/app/all", JSON.stringify(selfMsg));
            return true;
        } catch(e) {
            return false;
        }
    }

    render() {
        // const wsSourceUrl = window.location.protocol + "//" + window.location.host + "/handler";
        const wsSourceUrl =  "https://my-social-network-x.herokuapp.com/handler"
        return (
            <div>
                <TalkBox topic="topic" currentUserId={ this.randomUserId }
                         currentUser={ this.randomUserName } messages={ this.state.messages }
                         onSendMessage={ this.sendMessage } connected={ this.state.clientConnected }/>

                <Launcher
                    agentProfile={{
                        teamName: groupChat,
                    }}
                    onMessageWasSent={this.sendMessage}
                    messageList={this.state.messages}
                    isOpen={true}
                />


                <SockJsClient url={ wsSourceUrl } topics={["/topic/all"]}
                              onMessage={ this.onMessageReceive } ref={ (client) => { this.clientRef = client }}
                              onConnect={ () => { this.setState({ clientConnected: true }) } }
                              onDisconnect={ () => { this.setState({ clientConnected: false }) } }
                              debug={ false }/>
            </div>
        );
    }
}
