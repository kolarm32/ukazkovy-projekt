import React from 'react';
import "./newsfeed.css"
import ContextUser from "../appContext";
import Table from "react-bootstrap/Table";

export default class Search extends React.Component {
    static contextType = ContextUser;

    constructor(props) {
        super(props);

        this.state = {
            users: null,
            isLoading: true,
            interval: null
        }
    }

    componentDidMount = async () =>{
        await this.changeSearch()
        window.addEventListener('hashchange', this.changeSearch,false)
    }

    changeSearch = async ()=> {
        this.search = this.props.history.location.hash.replace("#", "");
        if (this.search === "") {
             await fetch('https://my-social-network-x.herokuapp.com/users')
                .then(response => response.json())
                .then(data => {
                        this.setState({users: [...new Set(data)], isLoading: false})
                    }
                )

        } else {
            await fetch('https://my-social-network-x.herokuapp.com/user/search/' + this.search)
                .then(response => response.json())
                .then(data => this.setState({users: data, isLoading: false}))
        }
    }


    render (){
        if (this.state.isLoading) {
            return <h3>Just a moment, loading...</h3>;
        }
        return (
            <main>
                <div className="searchResult">
                    <h3>Results:</h3>
                    {(this.state.users == null || this.state.users.length === 0) ? <h4> No results found. </h4> :
                        <Table striped bordered hover>
                            <thead>
                            <tr>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Email</th>
                            </tr>
                            </thead>
                            <tbody>
                            {this.state.users.map(user =>
                                <tr key={user.id}
                                    onClick={()=>{
                                        this.props.history.push("/viewProfile/id:" + user.id)
                                    }}>
                                    <td>{user.firstName}</td>
                                    <td>{user.lastName}</td>
                                    <td>{user.email}</td>
                                </tr>
                            )
                            }
                            </tbody>
                        </Table>
                    }
                </div>
            </main>);
    }

}