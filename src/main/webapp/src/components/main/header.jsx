import React from 'react';
import "../main/header.css";
import ContextUser from "../appContext";
import {Link, NavLink} from "react-router-dom";
import {Col, Form} from "react-bootstrap";
import Button from "react-bootstrap/Button";


export default class MainHeader extends React.Component {
    static contextType = ContextUser;

    constructor(props) {
        super(props);
        this.state = {
            user: {},
            users: {},
            searchInput: null
        }
    }

    componentDidMount() {
        this.setState({user: this.context.state.logged})
    }

    inputUpdateHandler = async (event, nameOfFormInput) => {
        const stringProperties = ["searchInput"];
        let newState = {...this.state.form};
        if (stringProperties.includes(nameOfFormInput)) {
            newState[nameOfFormInput] = event.target.value;
        }
        await this.setState({searchInput: newState});
    };

    submitHandler = async (event) => {
        event.preventDefault();
        if (this.state.searchInput == null) {
            window.location.href = "/search";
        } else
            window.location.href = "/search/#" + this.state.searchInput.searchInput;
    }


    render() {
        return <header className={"header"}>
            <nav className={"navbar navbar-expand-md navbar-dark bg-light sticky-top"}>
                <div className={"container-fluid"}>
                    <NavLink className={"navbar-brand"} exact to="/"><img src={"/images/logo.png"}
                                                                          alt={"logo"}/></NavLink>
                    <h1>
                        {this.state.user != null ? this.state.user.firstName + " " + this.state.user.lastName : null}
                    </h1>

                    <Form onSubmit={this.submitHandler}>
                        <Form.Group controlId={"searchInput"}>
                            <Form.Row>
                                <Col>
                                    <Form.Control type={"text"} placeholder={"Search"}
                                                  onChange={(event) => {
                                                      this.inputUpdateHandler(event, "searchInput")
                                                      // console.log(this.state.searchInput)
                                                      // window.location.href = "/search/#" + (this.state.searchInput == null ? "" : this.state.searchInput.searchInput);
                                                  }
                                                  }/>
                                </Col>
                                <Col>
                                    <Button type={"submit"}>Search</Button>
                                </Col>

                                {/*<NavLink className={"navbar-item"} to={this.state.searchInput == null ? "/search" : "/search/#" + this.state.searchInput.searchInput}>*/}
                                {/*    <Button type={"submit"}>Search</Button>*/}
                                {/*</NavLink>*/}
                            </Form.Row>
                        </Form.Group>
                    </Form>

                    <NavLink className={"navbar-item"} to="/profile">My Profile</NavLink>
                    <NavLink className={"navbar-item"} to="/messages">Messages</NavLink>
                    {/*<NavLink className={"navbar-item"} to="/groupChat">Group Chat</NavLink>*/}
                    <Link className={"navbar-item"} exact="true" to="/" onClick={() => {
                        this.context.actions.logout();
                    }}>Log Out</Link>
                </div>
            </nav>
        </header>;
    }
}

MainHeader.contextType = ContextUser;