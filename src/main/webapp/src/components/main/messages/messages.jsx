import React from "react";
import ContextUser from "../../appContext";
import ListGroup from "react-bootstrap/ListGroup";
import {Launcher} from 'react-chat-window'

export default class Messages extends React.Component {
    static contextType = ContextUser;

    constructor(props) {
        super(props);
        this.state = {
            user: null,
            friends: null,
            messageList: [],
            currentFriend: null,
            interval:null,
            openChat: false,
            newMessagesCount:0
        }
    }

    getConversation = async ()=>{
        if (this.state.currentFriend == null) return;
        let myMessages = []

        await fetch("https://my-social-network-x.herokuapp.com/getChat/" + parseInt(this.context.state.logged.id) +"/with/"+ parseInt(this.state.currentFriend.id), {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
            },
            credentials: "include",
        })
            .then((response) => {
                if (response.ok) {
                    return response.json();
                } else {
                    console.error("Chyba pri nacitani chatu " + response.statusText + "   " + response.status)
                    return null;
                }
            })
            .then((data) => {

                data.forEach(mess => {
                    myMessages.push({
                        author: "me",
                        type: 'text',
                        data: {
                            text: mess.text,
                            time:mess.sent
                        }
                    })
                })

            }).catch((err) => {
            console.log(err)
             console.log("nejakej error")
        });

        await fetch("https://my-social-network-x.herokuapp.com/getChat/" + parseInt(this.state.currentFriend.id) +"/with/"+ parseInt(this.context.state.logged.id), {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
            },
            credentials: "include",
        })
            .then((response) => {
                if (response.ok) {
                    return response.json();
                } else {
                    console.error("Chyba pri nacitani chatu " + response.statusText + "   " + response.status)
                    return null;
                }
            })
            .then((data) => {
                data.forEach(mess => {
                    myMessages.push({
                        author: "them",
                        type: 'text',
                        data: {
                            text: mess.text,
                            time:mess.sent
                        }
                    })
                })

            }).catch((err) => {
            console.log(err)
            console.log("nejakej error")
        });

        if(myMessages.length === this.state.messageList){
            return
        }
        this.setState({ newMessagesCount:myMessages.length });
        this.setState({messageList: myMessages.sort((a, b) => a.data.time > b.data.time)})
    }




    componentDidMount() {
        setInterval(this.getConversation,1000)

        fetch("https://my-social-network-x.herokuapp.com/user/friends", {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
            },
            credentials: "include",
        })
            .then((response) => {
                if (response.ok) {
                    return response.json();
                } else {
                    console.error("NEPODARILO SE prihlasit" + response.statusText + "   " + response.status)
                    this.context.actions.logout();
                    this.props.history.push("/");
                    return null
                }
            })
            .then((data) => {
                this.setState({friends: data})
            }).catch(() => {
            console.log("nejakej error")
        });
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }

    _onMessageWasSent(message) {
        this.setState({
            messageList: [...this.state.messageList, message]
        })

        if (message.data.text.length > 0) {

            let msg = {
                text:message.data.text,
                sent: new Date()
            }
            fetch('https://my-social-network-x.herokuapp.com/user/' + parseInt(this.context.state.logged.id) + '/addMessage/' + parseInt(this.state.currentFriend.id), {
                method: "POST",
                mode: "cors",
                credentials: "include",
                headers: {
                    "Content-Type": "application/json",
                },
                body: JSON.stringify(msg),
            }).then((response) => {
                console.log(response)
                if (response.ok) {
                    console.log("ok")
                } else {
                    console.log("Response --" + response.status);
                }
            }).catch(()=>{
                console.log("error")
            })
        }
    }


    _handleClick() {
        this.setState({
            openChat: !this.state.openChat,
            // newMessagesCount: 0
        });
    }


    render() {

        return (<div>
            <ListGroup>
                {this.state.friends != null ? this.state.friends.length === 0 ? <h4>You have no friends. QQ</h4>:
                    this.state.friends.map(friend =>
                        <ListGroup.Item
                            key={friend.id}
                            onClick={(id)=>{
                                this.setState({currentFriend: friend})
                                this.setState({openChat:false})
                                this.getConversation();
                                this.setState({openChat:true})
                            } }
                            variant={"primary"}
                        >
                            {friend.firstName + " " + friend.lastName + "(" + friend.email + ")"}



                        </ListGroup.Item>
                    ) : <h4>nic</h4>
                }
            </ListGroup>

            <Launcher
                agentProfile={{
                    teamName: this.state.currentFriend != null ? this.state.currentFriend.firstName + " " + this.state.currentFriend.lastName : null,
                    imageURL: "https://pasteboard.co/JbhU96Y.png"
                }}
                onMessageWasSent={this._onMessageWasSent.bind(this)}
                newMessagesCount={0}
                messageList={this.state.messageList}
                handleClick={this._handleClick.bind(this)}
                isOpen={this.state.openChat}
            />

        </div>);
    }

}

Messages.contextType = ContextUser;