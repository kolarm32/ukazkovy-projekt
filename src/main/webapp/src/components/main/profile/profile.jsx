import React from 'react';
import ProfileWall from "./profileWall";
import "./profile.css";
import ContextUser from "../../appContext";

export default class Profile extends React.Component {
    static contextType = ContextUser;

    componentDidMount() {
        this.setState({user : this.context.state.logged})
    }

    constructor(props) {
        super(props);
        this.state= {
            user: {}
        }
    }

    render() {
        if(this.context.state.logged == null){
            return (<div>
                <h2> You are not logged in!</h2>
            </div>);
        }
        return (
                <div className="profilePage">
                    <div className="profile">
                        <form method="post">
                            <div className="row">
                                <div className="col-md-4">
                                    <div className="profile-img">
                                        <img alt="profilePhoto" src={require("../../../profileplaceholder.jpg")}/>
                                        <div className="file btn btn-lg btn-primary">
                                            Change Photo
                                            <input type="file" name="file"/>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-6">
                                    <div className="profile-head">
                                        <h5>

                                        </h5>
                                        <h6>
                                            Description
                                        </h6>
                                    </div>
                                </div>
                                <div className="col-md-2">

                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-4">
                                    <div className="profile-work">
                                        <p>Hobbies</p>
                                        <p>skiing</p>
                                        <p>jogging</p>
                                        <p>skiing </p>
                                    </div>
                                </div>
                                <div className="col-md-8">
                                    <div className="tab-content profile-tab" id="myTabContent">
                                        <div className="tab-pane fade show active" id="home" role="tabpanel"
                                             aria-labelledby="home-tab">

                                            <div className="row">
                                                <div className="col-md-6">
                                                    <label>Name</label>
                                                </div>
                                                <div className="col-md-6">
                                                    <p>{this.context.state.logged.firstName}</p>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-md-6">
                                                    <label>Email</label>
                                                </div>
                                                <div className="col-md-6">
                                                    <p>{this.context.state.logged.email}</p>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-md-6">
                                                    <label>Phone</label>
                                                </div>
                                                <div className="col-md-6">
                                                    <p>123 456 7890</p>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-md-6">
                                                    <label>Profession</label>
                                                </div>
                                                <div className="col-md-6">
                                                    <p>Web Developer and Designer</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div>
                        <ProfileWall/>
                    </div>
                </div>
        );
    }

}

Profile.contextType = ContextUser;
