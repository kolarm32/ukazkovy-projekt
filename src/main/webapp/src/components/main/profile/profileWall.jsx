import React from 'react';
import "./profileWall.css"
import ContextUser from "../../appContext";
import {Form} from "react-bootstrap";

export default class ProfileWall extends React.Component {
    static contextType = ContextUser;

    constructor(props) {
        super(props);

        this.state = {
            user: {},
            posts: null,
            comment: {
                textContent: null,
            },
            isLoading: true,
            interval: null,
            postId: null
        }
    }

    componentDidMount() {
        let search = window.location.href;
        if (search.includes("viewProfile")) {
            let search = window.location.href;
            let userId = search.split(":")
            fetch('https://my-social-network-x.herokuapp.com/user/' + parseInt(userId[3]))
                .then(response=> response.json())
                .then(data=> this.setState({user: data, isLoading: false}))
        }
        else {
            this.setState({user : this.context.state.logged});
        }
        this.interval = setInterval((() => this.getPosts()), 5000);
    }

    getPosts = async () => {
        fetch('https://my-social-network-x.herokuapp.com/user/' + this.state.user.id + '/posts')
            .then(response => response.json())
            .then(data=> this.setState({posts: data, isLoading: false}))
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }


    inputUpdateHandler = async (event, nameOfFormInput) => {
        let newState = { ...this.state.form };
        newState["textContent"] = event.target.value;

        await this.setState({comment: newState});
        await this.setState({postId: nameOfFormInput});
    };


    submitHandler = async (event) => {
        event.preventDefault();
        this.setState({isValid : true});

        if (this.state.isValid) {
            fetch('https://my-social-network-x.herokuapp.com/post/' + parseInt(this.state.postId) + '/addComment', {
                method: "POST",
                mode: "cors",
                credentials: "include",
                headers: {
                    "Content-Type": "application/json",
                },
                body: JSON.stringify(this.state.comment),
            }).then(() => {
                console.log(this.state.comment)
                // if (response.ok) {
                //     return response.json();
                // } else {
                //     console.log("Response --" + response.status);
                // }
            })
        }
    }

    render(){
        if(this.state.isLoading){
            return <h3>Just a moment, loading...</h3>;
        }
        else if (this.state.posts === null || this.state.posts.length === 0) {
            return (
                <main>
                    <div className="posts">
                        <h4>
                            {this.state.user.id === this.context.state.logged.id ? "You haven't shared anything yet :(" : this.state.user.firstName + " hasn't shared anything yet!"}
                        </h4>
                    </div>
                </main>
            );
        }
        return (
            <main>
                <div className="posts">
                    <h4>
                        {this.state.user.id === this.context.state.logged.id ? "Your timeline" : this.state.user.firstName + "'s timeline"}
                    </h4>
                        {
                            this.state.posts.map(post =>
                            <Form key={post.id} className={"windows radius post"}>
                                <Form.Label>Shared {post.timestamp.substr(0, 10)}</Form.Label>
                                <Form.Group>
                                    <Form.Control as={"textarea"} readOnly={true}
                                                  value={post.postContent} disabled={true}>
                                    </Form.Control>
                                    {/*<Form onSubmit={this.submitHandler}>*/}
                                    {/*    <Form.Label>Add a comment</Form.Label>*/}
                                    {/*    <Form.Control as={"textarea"}*/}
                                    {/*                  onChange={(event) => this.inputUpdateHandler(event, post.id)}/>*/}
                                    {/*    <Button type={"submit"} >Share</Button>*/}
                                    {/*</Form>*/}
                                </Form.Group>
                            </Form>
                        )}
                </div>
            </main>);

    }
}