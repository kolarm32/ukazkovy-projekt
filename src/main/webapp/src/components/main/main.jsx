import React from 'react';
import "./main.css"
import {HashRouter, Route} from "react-router-dom";
import IndexHeader from "../IndexPage/Header";
import IndexBody from "../IndexPage/Body";
import Header from "./header";
import Newsfeed from "./newsfeed";
import Messages from "./messages/messages";
import Profile from "./profile/profile";

import User from "../Authorization/User";
import ContextUser from "../appContext";
import {Switch} from "react-bootstrap";

export default class Main extends React.Component {
    static contextType = ContextUser;

    constructor(props) {
        super(props);

        console.log(User);
        console.log(localStorage);
    }

    componentDidMount() {
        this.setState({user: this.context.state.logged})
    }

    render(){
        if (this.context.state.authenticated === true) {
            return (
                <HashRouter>
                    <div>
                        <Route component={Header}/>
                        <Switch>
                            <Route exact path="/" component={Newsfeed}/>
                            <Route path="/profile" component={Profile}/>
                            <Route path="/messages" component={Messages}/>
                        </Switch>
                    </div>
                </HashRouter>
            );
        }
        else {
            return (<div>
                    <Route exact path="/" component={IndexHeader}/>
                    <Route exact path="/" component={IndexBody}/>
                </div>
            );
        }
    }
}

Profile.contextType = ContextUser;