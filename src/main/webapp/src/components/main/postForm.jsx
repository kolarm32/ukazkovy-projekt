import React from 'react';
import "./postForm.css"
import {Button, Form} from "react-bootstrap";
import ContextUser from "../appContext";

export default class PostForm extends React.Component {
    static contextType = ContextUser;

    constructor(props) {
        super(props);

        this.state = {
            user: {},
            postContent: {},
            owner: {},
            isValid: {}
        }
    }

    componentDidMount() {
        this.setState({user : this.context.state.logged});
    }

    inputUpdateHandler = async (event, nameOfFormInput) => {
        this.setState({owner: this.context.state.logged});
        const stringProperties = ["postContent"];
        let newState = { ...this.state.form };
        if (stringProperties.includes(nameOfFormInput)) {
            newState[nameOfFormInput] = event.target.value;
        }
        await this.setState({postContent: newState});
    };

    submitHandler = async (event) => {
        event.preventDefault();
        this.setState({isValid : true});
        console.log(this.state);

        if (this.state.isValid) {
            fetch('https://my-social-network-x.herokuapp.com/user/' + this.state.user.id + '/sharePost', {
                method: "POST",
                mode: "cors",
                credentials: "include",
                headers: {
                    "Content-Type": "application/json",
                },
                body: JSON.stringify(this.state.postContent, this.state.owner),
            }).then((response) => {
                console.log(response)
                // if (response.ok) {
                //     return response.json();
                // } else {
                //     console.log("Response --" + response.status);
                // }
            }).then(data => {
                console.log(data);
            });
        }
    }

    render(){
        return <div className="postForm">
            <Form className={"windows radius"}
                  onSubmit={this.submitHandler}>
                <h3>Write a post</h3>
                <Form.Group controlId={"postContent"}>
                    <Form.Control as={"textarea"} rows={"5"} placeholder={"What's on your mind?"}
                                  onChange={(event) => this.inputUpdateHandler(event, "postContent")
                    }/>
                </Form.Group>
                <Button type={"submit"} >Share</Button>
            </Form>
        </div>
    }
}