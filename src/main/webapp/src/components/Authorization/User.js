import * as React from "react";

class User extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            userData: null,
            isLoading: true
        }
    }

    componentDidMount() {
        fetch('http://localhost:8080/user/current')
            .then(response=> response.json())
            .then(data=> this.setState({userData: data,isLoading: false}))
    }

    render() {
        return;
    }
}

export default User;