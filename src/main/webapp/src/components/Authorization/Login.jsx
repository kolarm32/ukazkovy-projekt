import * as React from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import ContextUser from "../appContext";
import Cookies from "js-cookie";
import MyAlert from "../MyAlert";

export default class Login extends React.Component {
    static contextType = ContextUser;
    _isMounted = false;

    constructor(props) {
        super(props);
        this.state = {
            user: {},
            form:{}
        }
    }

    componentDidMount() {
        this._isMounted = true
        if(Cookies.get("JSESSIONID") && this.state.user == null){
            this.context.actions.tryLogin();
        }
    }

    isLoggedIn = async() =>{
        return this.state.user != null;
    }


    inputUpdateHandler = async (event, nameOfFormInput) => {
        const stringProperties = ["email", "password"];
        let newState = { ...this.state.user };
        if (stringProperties.includes(nameOfFormInput)) {
            newState[nameOfFormInput] = event.target.value;
        }
        await this.setState({ user: newState });
    };


    submitHandler = async (event) => {
        event.preventDefault();
        this.state.form.isValid = true;
        console.log(this.state.user);

        if (this.state.form.isValid) {
            fetch("https://my-social-network-x.herokuapp.com/login", {
                method: "POST",
                mode: "cors",
                credentials: "include",
                headers: {
                    "Content-Type": "application/json",
                },
                body: JSON.stringify(this.state.user),
            }).then((response) => {
                if (response.ok){
                    return response.json();
                } else {
                    this.badLogin = true;
                    throw Error(response.status);
                }
            }).then((data)=>{
                console.log("Udaje" +data);
                if (this._isMounted) {
                    this.setState({user:data});
                    this.context.actions.updateLogged(data);
                    this.context.actions.updateAuth(true);
                }
            }).catch((err)=>{
                this.context.actions.logout();
                console.log(err)
            });
        }
    };


    render() {
        let loginNotSuccess = null;
        if(this.context.state.authenticated === true){
            return <div><h3>Logged as {this.context.state.logged.firstName}</h3></div>
        }

        return (
            <Container className="login_container">
                    <Form
                        className="window radius login_form"
                        onSubmit={this.submitHandler}
                    >
                <Row>
                        <Form.Group controlId="formBasicEmail">
                            <Form.Label>Email address</Form.Label>
                            <Form.Control
                                type="email"
                                placeholder="Enter email"
                                onChange={(event) =>
                                    this.inputUpdateHandler(event, "email")
                                }
                            />
                        </Form.Group>

                        <Form.Group controlId="formBasicPassword">
                            <Form.Label>Password</Form.Label>

                            <Form.Control
                                type="password"
                                placeholder="Password"
                                onChange={(event) =>
                                    this.inputUpdateHandler(event, "password")
                                }
                            />

                        <Button
                            variant="primary"
                            type="submit"
                            className="submit"
                        >
                            Login
                        </Button>

                        </Form.Group>
                        <div id={"alert"}> </div>
                        {this.badLogin === true ? <MyAlert variant="danger" text="Bad credentials" flash={true}/> : null}
                    </Row>
                </Form>
                {loginNotSuccess}
            </Container>
        );
    }
}