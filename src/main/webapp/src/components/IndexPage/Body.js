import React from "react";
import 'bootstrap/dist/css/bootstrap.css';
import Button from "react-bootstrap/Button";
import ContextUser from "../appContext";
import {Col, Form} from "react-bootstrap";

class Body extends React.Component {
    static contextType = ContextUser;

    constructor(props) {
        super(props);
        this.state = {
            user: {
            },
            form:{}
        }
    }

    componentDidMount() {
        // const response = await fetch('http://localhost:8080/test/companies');
        // const data = await response.json();
        // console.log(data);
        // this.setState({ trips: data });
    }
    state = { trips: null };

    inputUpdateHandler = async (event, nameOfFormInput) => {
        const stringProperties = ["email", "firstName", "lastName", "password", "birthDate", "gender"];
        let newState = { ...this.state.user };
        if (stringProperties.includes(nameOfFormInput)) {
            newState[nameOfFormInput] = event.target.value;
        }
        await this.setState({ user: newState });
    };

    submitHandler = async (event) => {
        event.preventDefault();
        //TODO - VALIDATION
        this.state.form.isValid = true;
        if (this.state.user.gender !== undefined) {
            this.state.user.gender = this.state.user.gender.toUpperCase();
        }
        console.log(this.state.user);

        if (this.state.form.isValid) {
            fetch("http://localhost:8080/signup", {
                method: "POST",
                mode: "cors",
                credentials: "include",
                headers: {
                    "Content-Type": "application/json",
                },
                body: JSON.stringify(this.state.user),
            }).then((response) => {
                console.log(response)
                //TODO - osetrenie vynimiek
                if (response.ok){
                    // this.props.history.push("/");
                    // reload();
                    return response.json();
                } else {
                    console.log("Response --"+ response.status );
                }
            }).then(data => {
                console.log(data);
            });
        }
    }

    render() {
        return (
            <main className={"container singup-form"}>
                <h1>Create a new account</h1>
                <Form className={"windows radius"}
                onSubmit={this.submitHandler}>
                    <Form.Group controlId={"email"}>
                        <Form.Label>
                            E-mail:
                        </Form.Label>
                        <Form.Control
                            type="email"
                            placeholder="E-mail"
                            onChange={(event) =>
                                this.inputUpdateHandler(event, "email")
                            }
                        />
                    </Form.Group>
                    <Form.Row>
                        <Col>
                            <Form.Group controlId={"firstName"}>
                                <Form.Label>
                                    First Name:
                                </Form.Label>
                                <Form.Control
                                    type="firstName"
                                    placeholder="First Name"
                                    onChange={(event) =>
                                        this.inputUpdateHandler(event, "firstName")
                                    }
                                />
                            </Form.Group>
                        </Col>
                        <Col>
                            <Form.Group controlId={"lastName"}>
                                <Form.Label>
                                    Last Name:
                                </Form.Label>
                                <Form.Control
                                    type="lastName"
                                    placeholder="Last Name"
                                    onChange={(event) =>
                                        this.inputUpdateHandler(event, "lastName")
                                    }
                                />
                            </Form.Group>
                        </Col>
                    </Form.Row>
                    <Form.Row>
                        <Col>
                            <Form.Group controlId={"password"}>
                                <Form.Label>
                                    Password:
                                </Form.Label>
                                <Form.Control
                                    type="password"
                                    placeholder="Password"
                                    onChange={(event) =>
                                        this.inputUpdateHandler(event, "password")
                                    }
                                />
                            </Form.Group>
                        </Col>
                        <Col>
                            <Form.Group controlId={"password2"}>
                                <Form.Label>
                                    Password:
                                </Form.Label>
                                <Form.Control
                                    type="password"
                                    placeholder="Password"
                                    // onChange={(event) =>
                                    //     this.inputUpdateHandler(event, "password")
                                    // }
                                />
                            </Form.Group>
                        </Col>
                    </Form.Row>
                    <Form.Row>
                        <Col>
                            <Form.Group controlId={"birthDate"}>
                                <Form.Label>
                                    Birth Date:
                                </Form.Label>
                                <Form.Control size={"sm"}
                                    type="date"
                                    onChange={(event) =>
                                        this.inputUpdateHandler(event, "birthDate")
                                    }
                                />
                            </Form.Group>
                        </Col>
                        <Col>
                            <Form.Group controlId={"gender"}>
                                <Form.Label>
                                    Gender:
                                </Form.Label>
                                <Form.Control as={"select"} size={"sm"}
                                    onChange={(event) =>
                                        this.inputUpdateHandler(event, "gender")
                                    }>
                                    <option>Male</option>
                                    <option>Female</option>
                                    <option>Other</option>
                                </Form.Control>
                            </Form.Group>
                        </Col>
                    </Form.Row>
                    <Form.Row>
                        <Col>
                            <Form.Group>
                                <Form.Check/>
                                <Form.Label>
                                    Agree to terms and conditions.
                                </Form.Label>
                            </Form.Group>
                        </Col>
                    </Form.Row>
                    <Button type={"submit"}>Sign Up</Button>
                </Form>
            </main>
        );
    }
}

export default Body;