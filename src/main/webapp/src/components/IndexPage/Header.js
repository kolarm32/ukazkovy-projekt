import React from "react";
import 'bootstrap/dist/css/bootstrap.css';
import ContextUser from "../appContext";
import Login from "../Authorization/Login";
import {Route} from "react-router-dom";
import MainHeader from "../main/header";


export default class Header extends React.Component {
    static contextType = ContextUser;

    componentDidMount() {
    }

    render() {
        if(this.context.state.authenticated ===true){
            return <MainHeader/>
        }
        return <header className={"header"}>
            <nav className={"navbar navbar-expand-md navbar-dark bg-light sticky-top"}>
                <div className={"container-fluid"}>
                    <a className={"navbar-brand"} href={"/main"}><img alt="xx" className={"icon"} src={"/images/logo.png"}/> </a>
                    <h1>My Social Network</h1>
                    <div className={"loginForm"}>
                        {<Route component={Login}/>}
                    </div>
                </div>
            </nav>
        </header>
    }
}

Header.contextType = ContextUser;