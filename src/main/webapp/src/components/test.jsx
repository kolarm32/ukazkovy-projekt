import React from 'react';


export default class Test extends React.Component{
    constructor(props) {
        super(props);

        this.state = {
            accounts: null,
            isLoading: true
        }
    }

    componentDidMount() {
        fetch('http://localhost:8080/accounts')
            .then(response=> response.json())
            .then(data=> this.setState({accounts: data,isLoading: false}))
    }

    render() {
        if(this.state.isLoading){
            return <h3>Still loading</h3>;
        }
        console.log(this.state.accounts);
        return <div>
            <table className="table table-dark">
                <thead>
                <tr>
                    <th scope={"col"}> id</th>
                    <th scope={"col"}> First Name</th>
                    <th scope={"col"}> Last Name</th>
                    <th scope={"col"}> Email</th>
                    <th scope={"col"}> Friends</th>
                    <th scope={"col"}> Birthdate</th>
                    <th scope={"col"}> Gender</th>
                </tr>
                </thead>

                <tbody>
                {this.state.accounts.map(acc =>
                    <tr key={acc.id}>
                        <th scope={"row"}>{acc.id} </th>
                        <td> {acc.firstName} </td>
                        <td> {acc.lastName} </td>
                        <td> {acc.email} </td>
                        <td> {acc.friends} </td>
                        <td> {acc.birthDate} </td>
                        <td> {acc.gender} </td>
                    </tr>
                )}
                </tbody>

            </table>
        </div>

    }
}