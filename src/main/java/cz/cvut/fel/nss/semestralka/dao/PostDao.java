package cz.cvut.fel.nss.semestralka.dao;

import cz.cvut.fel.nss.semestralka.model.Post;
import cz.cvut.fel.nss.semestralka.model.User;
import org.springframework.stereotype.Repository;

import javax.persistence.PersistenceException;
import java.util.List;

@Repository
public class PostDao extends Dao<Post> {

    protected PostDao() {
        super(Post.class);
    }

    public List getMyPosts(User account) {
        try {
            return em.createQuery("SELECT p FROM Post p WHERE p.owner = ?1").setParameter(1, account).getResultList();
        } catch (RuntimeException e) {
            throw new PersistenceException(e);
        }
    }

    /*
    public List getMyFriendsPosts(Account account) {
        try {
            return em.createQuery("SELECT p FROM Post AS p JOIN Account AS a ON (p.owner = a.friends) WHERE p.owner = ?").setParameter(1, account.getFriends()).getResultList();
        } catch (RuntimeException e) {
            throw new PersistenceException(e);
        }
    }
     */

}
