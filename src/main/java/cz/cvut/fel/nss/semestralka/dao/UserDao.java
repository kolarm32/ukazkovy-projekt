package cz.cvut.fel.nss.semestralka.dao;

import cz.cvut.fel.nss.semestralka.model.Role;
import cz.cvut.fel.nss.semestralka.model.User;
import cz.cvut.fel.nss.semestralka.model.Profile;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import java.util.Date;
import java.util.List;

@Repository
public class UserDao extends Dao<User> {

    protected UserDao() {
        super(User.class);
    }

    public User findByEmail(String email) {
        try{
            return em.createNamedQuery("User.findByEmail", User.class).setParameter("email",email).getSingleResult();
        }catch( NoResultException e){
            return null;
        }
    }

    public List<User> getFriends(User acc){
        try{
            return em.createNamedQuery("User.getFriends", User.class).setParameter("email",acc.getEmail()).getResultList();
        }catch( NoResultException e){
            return null;
        }
    }

    public List<User> getAllUsers(){
        try{
            return em.createNamedQuery("User.getAllUsers", User.class).getResultList();
        }catch(NoResultException e){
            return null;
        }
    }

    public User create(String email, String password, String firstName, String lastName, Date birthDate, User.Gender gender) {
        User user = new User(birthDate, firstName, lastName, gender, email, password);
        persist(user);
        return user;
    }

    public User create(User user) {
        user.setRole(Role.USER);
        persist(user);
        return user;
    }
}
