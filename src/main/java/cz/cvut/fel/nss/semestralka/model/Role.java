package cz.cvut.fel.nss.semestralka.model;

public enum Role {
    USER("ROLE_USER"), ADMIN("ROLE_ADMIN");

    private final String role;

    Role(String role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return role;
    }
}
