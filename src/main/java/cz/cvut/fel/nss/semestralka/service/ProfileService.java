package cz.cvut.fel.nss.semestralka.service;

import cz.cvut.fel.nss.semestralka.dao.ProfileDao;
import cz.cvut.fel.nss.semestralka.model.Profile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;

@Service
public class ProfileService {
    
    private ProfileDao dao;

    @Autowired
    public ProfileService(ProfileDao dao) {
        this.dao = dao;
    }

    @Transactional
    public List<Profile> findAll(){
        return dao.findAll();
    }

    @Transactional
    public void update(Profile Profile) {
        dao.update(Profile);
    }
}
