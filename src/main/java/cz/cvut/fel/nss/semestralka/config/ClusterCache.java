package cz.cvut.fel.nss.semestralka.config;

import com.hazelcast.config.Config;
import com.hazelcast.config.FileSystemXmlConfig;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import cz.cvut.fel.nss.semestralka.Main;

import java.io.File;
import java.io.FileNotFoundException;

public class ClusterCache {

    private static HazelcastInstance instance;

    public static void init() {
        Config config = new Config();

        try {
            config = new FileSystemXmlConfig(new File(Main.class.getClassLoader().getResource("hazelcast.xml").getFile()));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        instance = Hazelcast.newHazelcastInstance(config);
    }

    public static void destroy() {
        instance.shutdown();
    }

    public static HazelcastInstance getInstance() {

        if (instance == null) {
            init();
        }

        return instance;
    }
}