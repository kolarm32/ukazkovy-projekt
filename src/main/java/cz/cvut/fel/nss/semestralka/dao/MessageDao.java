package cz.cvut.fel.nss.semestralka.dao;

import cz.cvut.fel.nss.semestralka.model.Message;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import java.util.List;

@Repository
public class MessageDao extends Dao<Message> {

    protected MessageDao() {
        super(Message.class);
    }


    public List getAllMessages() {
        try{
            return em.createNamedQuery("Message.getMessages", Message.class)
                .getResultList();
        }catch( NoResultException e){
            return null;
        }
    }

}
