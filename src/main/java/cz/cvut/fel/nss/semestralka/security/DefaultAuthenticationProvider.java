package cz.cvut.fel.nss.semestralka.security;

import cz.cvut.fel.nss.semestralka.security.model.UserDetails;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import cz.cvut.fel.nss.semestralka.security.model.AuthenticationToken;

import java.sql.SQLOutput;

@Service
public class DefaultAuthenticationProvider implements AuthenticationProvider {

    private final UserDetailsService userDetailsService;

    private final PasswordEncoder passwordEncoder;

    @Autowired
    public DefaultAuthenticationProvider(UserDetailsService userDetailsService, PasswordEncoder passwordEncoder) {
        this.userDetailsService = userDetailsService;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        final UserDetails userDetails = (UserDetails) userDetailsService.loadUserByUsername(authentication.getPrincipal().toString());

        final String password = (String) authentication.getCredentials();

        if (!passwordEncoder.matches(password, userDetails.getPassword())){
            throw new BadCredentialsException("Provided credentials don't match.");
        }
//        if(!password.equals(userDetails.getPassword())){
//            throw new BadCredentialsException("Provided credentials don't match.");
//        }

        userDetails.eraseCredentials(); // Don't pass credentials around in the user details object
//        final AuthenticationToken token = new AuthenticationToken(userDetails.getAuthorities(), userDetails);
//        token.setAuthenticated(true);
//        token.setDetails(userDetails);
//        System.out.println("Authenticated");
//        final SecurityContext context = new SecurityContextImpl();
//        context.setAuthentication(token);
//        SecurityContextHolder.setContext(context);
        return SecurityUtils.setCurrentUser(userDetails);
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return UsernamePasswordAuthenticationToken.class.isAssignableFrom(aClass) ||
                AuthenticationToken.class.isAssignableFrom(aClass);
    }
}
