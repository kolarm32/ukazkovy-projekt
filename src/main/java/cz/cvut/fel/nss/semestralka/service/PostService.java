package cz.cvut.fel.nss.semestralka.service;

import cz.cvut.fel.nss.semestralka.dao.PostDao;
import cz.cvut.fel.nss.semestralka.model.Post;
import cz.cvut.fel.nss.semestralka.model.Post;
import cz.cvut.fel.nss.semestralka.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;

@Service
public class PostService {

    private PostDao dao;

    @Autowired
    public PostService(PostDao postDao){
        this.dao = postDao;
    }

    @Transactional
    public Post find(Integer id) {
        return dao.find(id);
    }

    @Transactional
    public List<Post> findAll(){
        return dao.findAll();
    }

    @Transactional
    public void persist(Post Post) {
        dao.persist(Post);
    }

    @Transactional
    public void update(Post Post) {
        dao.update(Post);
    }

    @Transactional
    public void remove(Post Post) {
        Objects.requireNonNull(Post);
        dao.remove(Post);
    }

    @Transactional
    public List<Post> getMyPosts(User user) {
        return dao.getMyPosts(user);
    }

}
