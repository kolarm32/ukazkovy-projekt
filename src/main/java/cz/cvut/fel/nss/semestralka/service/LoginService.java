package cz.cvut.fel.nss.semestralka.service;

import cz.cvut.fel.nss.semestralka.dao.UserDao;
import cz.cvut.fel.nss.semestralka.model.User;
import cz.cvut.fel.nss.semestralka.security.DefaultAuthenticationProvider;
import cz.cvut.fel.nss.semestralka.security.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class LoginService {

    private DefaultAuthenticationProvider provider;
    private UserDao userDao;

    @Autowired
    public LoginService(DefaultAuthenticationProvider provider, UserDao userDao) {
        this.provider = provider;
        this.userDao = userDao;
    }


    @Transactional(readOnly = true)
    public User login(String email, String password) throws Exception {
        if (SecurityUtils.getCurrentUserDetails() != null) throw new Exception("Already logged in");
        Authentication auth = new UsernamePasswordAuthenticationToken(email, password);
        provider.authenticate(auth);
        return userDao.find(SecurityUtils.getCurrentUser().getId());
    }

}
