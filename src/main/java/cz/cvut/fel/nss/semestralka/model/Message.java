package cz.cvut.fel.nss.semestralka.model;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Date;

@Entity
@NamedQuery(name = "Message.getMessages", query = "SELECT m FROM Message m")
public class Message extends AbstractEntity {

    @Column
    @Size(min = 1, max = 100)
    private String text;

    @Column
    private Date sent;

    public Message() {}

    public Message(String text) {
        this.text = text;
        this.sent = new Date();
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getSent() {
        return sent;
    }

    public void setSent(Date sent) {
        this.sent = sent;
    }
}
