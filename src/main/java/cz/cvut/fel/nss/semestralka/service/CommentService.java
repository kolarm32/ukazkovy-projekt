package cz.cvut.fel.nss.semestralka.service;

import cz.cvut.fel.nss.semestralka.dao.CommentDao;
import cz.cvut.fel.nss.semestralka.model.Comment;
import cz.cvut.fel.nss.semestralka.model.Post;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;

@Service
public class CommentService {

    private final CommentDao dao;

    @Autowired
    public CommentService(CommentDao dao) {
        this.dao = dao;
    }

    @Transactional
    public Comment find(Integer id) {
        return dao.find(id);
    }

    @Transactional
    public List<Comment> findAll(){
        return dao.findAll();
    }

    @Transactional
    public void persist(Comment comment) {
        dao.persist(comment);
    }

    @Transactional
    public void update(Comment comment) {
        dao.update(comment);
    }

    @Transactional
    public void remove(Comment comment) {
        Objects.requireNonNull(comment);
        dao.remove(comment);
    }

    @Transactional
    public List<Comment> getCommentsOnPost(Post post){
        return dao.getComments(post);
    }
}
