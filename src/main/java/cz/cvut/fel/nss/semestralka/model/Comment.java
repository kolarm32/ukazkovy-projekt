package cz.cvut.fel.nss.semestralka.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Date;

@Entity
@NamedQuery(name = "Comment.getComments", query = "SELECT c from Comment c WHERE c.post=:postID")
public class Comment extends AbstractEntity{

    @ManyToOne(cascade = CascadeType.ALL)
    @JsonIgnore
    private User owner;

    @Column
    private Date created;

    @ManyToOne
    private Post post;

    @Column
    @Size(min = 1, max = 200)
    private String textContent;

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public String getTextContent() {
        return textContent;
    }

    public void setTextContent(String textContent) {
        this.textContent = textContent;
    }

    @Override
    public String toString() {
        return "Comment{" +
                "owner=" + owner +
                ", created=" + created +
                ", post=" + post +
                ", textContent='" + textContent + '\'' +
                '}';
    }
}
