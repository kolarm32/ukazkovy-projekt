package cz.cvut.fel.nss.semestralka.model;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.*;

@Entity
@NamedQueries({
    @NamedQuery(name =  "User.findByEmail", query = "Select u FROM User u WHERE :email = u.email"),
    @NamedQuery(name =  "User.getFriends", query = "Select u FROM User u WHERE :email = u.email"),
    @NamedQuery(name =  "User.getAllUsers", query = "Select u FROM User u")
})
@Table(name="Users")
public class User extends AbstractEntity implements Serializable {

    @Column
    private Date birthDate;

    @Column(nullable = false)
    @Size(min = 1, max = 30)
    private String firstName;

    @Column(nullable = false)
    @Size(min = 1, max = 30)
    private String lastName;

    @Enumerated
    private User.Gender gender;

    @OneToOne(cascade = CascadeType.ALL)
    private Profile profile;

    public Profile getProfile() {
        return profile;
    }

    @Column(nullable = false)
    @Size(min = 1, max = 30)
    private String email;

    @Column(nullable = false)
    private Role role;

    @Column(nullable = false)
    private String password;

    @ElementCollection
    private Set<String> friends;

//    @MapKey(name="email")
    // email, messages
    @OneToMany(mappedBy="sender", cascade = CascadeType.ALL)
    private Map<String, MessageType> chat = new HashMap<>();

    @Column
    private boolean isBlocked = false;

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public User() {
        this.friends = new HashSet<>();
    }

    public void setFriends(Set<String> friends) {
        this.friends = friends;
    }

    public void addMessage(String email, MessageType ms){
        chat.put(email,ms);
    }

    public Map<String, MessageType> getChat() {
        return chat;
    }

    public void setChat(Map<String, MessageType> chat) {
        this.chat = chat;
    }

    public User(Date birthDate, String firstName, String lastName, Gender gender, String email, String password) {
        super();
        this.birthDate = birthDate;
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.email = email;
        this.password = password;
        this.role = Role.USER;
        this.friends = new HashSet<>();
        this.profile = new Profile();
    }

    public void addFriend(User user) {
        this.friends.add(user.getEmail());
    }

    public void deleteFriend(String noMoreFriend) {
        this.friends.remove(noMoreFriend);
    }

    public enum Gender{
        MALE,FEMALE,OTHER
    }

    public void encodePassword(PasswordEncoder encoder) {
        this.password = encoder.encode(password);
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getFirstName() {
        return firstName;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public User.Gender getGender() {
        return gender;
    }

    public void setGender(User.Gender gender) {
        this.gender = gender;
    }

    public boolean isBlocked() {
        return isBlocked;
    }

    public void setBlocked(boolean blocked) {
        isBlocked = blocked;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<String> getFriends() {
        return friends;
    }

    public void erasePassword() {
        password = null;
    }

    @Override
    public String toString() {
        return "User{" +
                "birthDate=" + birthDate +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", gender=" + gender +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
