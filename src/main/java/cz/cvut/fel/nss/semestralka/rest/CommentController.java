package cz.cvut.fel.nss.semestralka.rest;

import cz.cvut.fel.nss.semestralka.model.Comment;
import cz.cvut.fel.nss.semestralka.model.Post;
import cz.cvut.fel.nss.semestralka.service.CommentService;
import cz.cvut.fel.nss.semestralka.service.PostService;
import cz.cvut.fel.nss.semestralka.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

@RestController
@CrossOrigin(origins = "https://my-social-network-x.herokuapp.com/", allowCredentials="true", allowedHeaders = "*")
public class CommentController {

    private UserService userService;
    private final CommentService commentService;
    private final PostService postService;

    @Autowired
    public CommentController(CommentService commentService, PostService postService, UserService userService) {
        this.commentService = commentService;
        this.postService = postService;
        this.userService = userService;
    }

    @PostMapping(value = "/post/{postId}/addComment",produces = MediaType.APPLICATION_JSON_VALUE)
    public void addComment(@RequestBody Comment comment, @PathVariable Integer postId){
//        Post p = postService.find(postId);
//        comment.setPost(p);
//        p.getComments().add(comment);
//        postService.update(p);
    }

    @GetMapping(value = "/comments",produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Comment> getComments(){
        return commentService.findAll();
    }


    @GetMapping(value = "/comment/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
    public Comment getComment(@PathVariable Integer id){
        return commentService.find(id);
    }

    @GetMapping(value = "/post/{postId}/comments")
    public List<Comment> getCommentOnPost(@PathVariable Integer postId){
        // TODO: vytvorit postservice
        return null;
        //return commentService.getCommentsOnPost()
    }



}
