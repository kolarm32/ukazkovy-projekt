package cz.cvut.fel.nss.semestralka.rest;
import cz.cvut.fel.nss.semestralka.model.Message;
import cz.cvut.fel.nss.semestralka.model.User;
import cz.cvut.fel.nss.semestralka.service.MessageService;
import cz.cvut.fel.nss.semestralka.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

@RestController
@CrossOrigin(origins = "https://my-social-network-x.herokuapp.com/", allowCredentials="true", allowedHeaders = "*")
public class MessageController {

    private MessageService messageService;
    private UserService userService;

    @Autowired
    public MessageController(MessageService messageService,UserService userService ) {
        this.messageService = messageService;
        this.userService = userService;
    }


    @PostMapping(value = "/addMessage/user/{id}/{id2}",produces = MediaType.APPLICATION_JSON_VALUE)
    public void addMessage(@PathVariable("id") Integer me, @PathVariable("id2")Integer friend, @RequestBody Message message) {
        User meUser = userService.find(me);
        User friendUser = userService.find(friend);
        Objects.requireNonNull(meUser);
        // TODO

    }

    @GetMapping(value = "/messages", produces = MediaType.APPLICATION_JSON_VALUE)
    public List getAllMessages(){
        return messageService.getAllmessages();
    }
}
