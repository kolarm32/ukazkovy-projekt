package cz.cvut.fel.nss.semestralka.model;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Profile extends AbstractEntity{

    @Column
    @Size(min = 1, max = 30)
    private String school;

    @Column
    @Size(min = 1, max = 30)
    private String work;

    @Column
    @Size(min = 1, max = 100)
    private String hobbies;

    @Column
    @Size(min = 1, max = 150)
    private String profileDescription;

    @Column
    private File photo;

    @OneToOne
    private User user;

    @OneToMany(mappedBy = "profile")
    private List<Post> timeline;

    public Profile() {
        timeline = new ArrayList<>();
    }

    public Profile(User user) {
        this.user = user;
        timeline = new ArrayList<>();
    }

    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public String getWork() {
        return work;
    }

    public void setWork(String work) {
        this.work = work;
    }

    public String getHobbies() {
        return hobbies;
    }

    public void setHobbies(String hobbies) {
        this.hobbies = hobbies;
    }

    public String getProfileDescription() {
        return profileDescription;
    }

    public void setProfileDescription(String profileDescription) {
        this.profileDescription = profileDescription;
    }

    public File getPhoto() {
        return photo;
    }

    public void setPhoto(File photo) {
        this.photo = photo;
    }

    public List<Post> getTimeline() {
        return timeline;
    }

    public void setTimeline(List<Post> timeline) {
        this.timeline = timeline;
    }
}
