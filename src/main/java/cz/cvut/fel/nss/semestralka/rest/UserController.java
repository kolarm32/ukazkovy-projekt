package cz.cvut.fel.nss.semestralka.rest;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import cz.cvut.fel.nss.semestralka.config.ClusterCache;
import cz.cvut.fel.nss.semestralka.model.Message;
import cz.cvut.fel.nss.semestralka.model.Role;
import cz.cvut.fel.nss.semestralka.model.User;
import cz.cvut.fel.nss.semestralka.security.SecurityUtils;
import cz.cvut.fel.nss.semestralka.service.UserService;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.MultiSearchResponse;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.common.unit.Fuzziness;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

import static org.elasticsearch.common.xcontent.XContentFactory.jsonBuilder;

@RestController
@CrossOrigin(origins = "https://my-social-network-x.herokuapp.com/", allowCredentials="true", allowedHeaders = "*")
public class UserController {

    private final UserService userService;

//    private final IMap<Long, User> map;

    @Autowired
    private Client client;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
//        HazelcastInstance instance = ClusterCache.getInstance();
//        map = instance.getMap("users-map");
    }

    @PostMapping(value = "/signup",produces = MediaType.APPLICATION_JSON_VALUE)
    public void signUp(@RequestBody User user) throws IOException {
        user.setRole(Role.USER);
        userService.persist(user);
    }

    @GetMapping(value = "/users",produces = MediaType.APPLICATION_JSON_VALUE)
    public List<User> getUsers(){
        return userService.getAllAccounts();
    }

    @GetMapping(value = "/user/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
    public User getById(@PathVariable Integer id){
//        if (map.containsKey(id.longValue())) {
//            return map.get(id.longValue());
//        }
//        return null;
        return userService.find(id);
    }


    @GetMapping(value = "/user/friends",produces = MediaType.APPLICATION_JSON_VALUE)
    public List<User> getFriends(){
        Set<String> friends = SecurityUtils.getCurrentUser().getFriends();
        List<User> resFriends = new ArrayList<>();
        for (String str:  friends) {
            resFriends.add(userService.findUserByEmail(str));
        }
        return resFriends;
    }

    @GetMapping(value = "/user/current", produces = MediaType.APPLICATION_JSON_VALUE)
    public User showCurrentUser() throws Exception {
        return userService.showCurrentUser();
    }

    @PostMapping(value = "/user/{id}/addFriend/{userid}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public void addFriend(@PathVariable("id") Integer me, @PathVariable("userid") Integer friend){
        User meUser = userService.find(me);
        User friendUser = userService.find(friend);
        Objects.requireNonNull(meUser);
        Objects.requireNonNull(friendUser);
        userService.addFriend(meUser,friendUser);
    }

    @PostMapping(value = "/user/{id}/removeFriend/{userid}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public void removeFriend(@PathVariable("id") Integer me, @PathVariable("userid") Integer friend){
        User meUser = userService.find(me);
        User friendUser = userService.find(friend);
        Objects.requireNonNull(meUser);
        Objects.requireNonNull(friendUser);
        userService.deleteFriend(meUser,friendUser);
    }
    
    @PostMapping(value="/user/{id}/addMessage/{friendId}")
    @ResponseStatus(HttpStatus.OK)
    public void addMessage(@PathVariable("id") Integer id, @PathVariable("friendId") Integer friendId,@RequestBody Message message){
        User me = userService.find(id);
        User friend = userService.find(friendId);
        Objects.requireNonNull(me);
        Objects.requireNonNull(friend);
        userService.sendMessage(me,friend.getEmail(),message);
    }

    @GetMapping(value = "/getChat/{userId}/with/{friendId}")
    public List<Message> getAllMessages(@PathVariable("userId") Integer user, @PathVariable("friendId") Integer friendid){
        User me = userService.find(user);
        User friend = userService.find(friendid);
        Objects.requireNonNull(me);
        Objects.requireNonNull(friend);
        return me.getChat().get(friend.getEmail()).getMessages();
    }


//    elasticsearch

    @GetMapping("/{id}")
    public Map<String, Object> view(@PathVariable("id") final String id) {
        GetResponse getResponse = client.prepareGet("users", "users", id).get();
        return getResponse.getSource();
    }

    @PostMapping("/create")
    public String create(@RequestBody User user) throws IOException {
        IndexResponse response = client.prepareIndex("users", "users", String.valueOf(user.getId()))
                .setType("users")
                .setSource(jsonBuilder().startObject()
                        .field("id",user.getId())
                        .field("firstName", user.getFirstName())
                        .field("lastName", user.getLastName())
                        .field("email",user.getEmail())
                        .endObject()
                ).get();
        System.out.println("User added to elastic");
//        map.put(user.getId().longValue(), user);
        return response.getResult().toString();
    }

    @PutMapping("/update/{id}")
    public String update(@PathVariable("id") Integer id, @RequestBody User user) throws IOException {
        UpdateRequest updateRequest = new UpdateRequest();
        updateRequest.index("users").type("users").id(String.valueOf(id))
                .doc(jsonBuilder().startObject()
                    .field("id",user.getId())
                    .field("firstName", user.getFirstName())
                    .field("lastName", user.getLastName())
                    .field("email",user.getEmail())
                    .endObject());

        try {
            UpdateResponse updateResponse = client.update(updateRequest).get();
            return updateResponse.status().toString();
        } catch (InterruptedException | ExecutionException e) { System.out.println(e); }

        return "Updated";
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/delete/{id}")
    public String delete(@PathVariable final String id) {
        DeleteResponse deleteResponse = client.prepareDelete("users", "users", id).get();
        return deleteResponse.getResult().toString();
    }



    @GetMapping(value = "/user/search/{string}")
    public List<Map<String, Object>> search(@PathVariable("string") String string){
        Objects.requireNonNull(string);

        QueryBuilder matchQueryBuilder = QueryBuilders.matchQuery("firstName", string)
                .fuzziness(Fuzziness.TWO);


        QueryBuilder matchQueryBuilder1 = QueryBuilders.matchQuery("lastName", string)
                .fuzziness(Fuzziness.TWO);

        QueryBuilder matchQueryBuilder3 = QueryBuilders.matchQuery("email", string)
                .fuzziness(Fuzziness.TWO);

        SearchRequestBuilder srb1 = client
                .prepareSearch()
                .setSearchType(SearchType.QUERY_THEN_FETCH)
                 .setQuery(matchQueryBuilder);
        SearchRequestBuilder srb2 = client
                .prepareSearch()
                .setSearchType(SearchType.QUERY_THEN_FETCH)
                .setQuery(matchQueryBuilder1);

        SearchRequestBuilder srb3 = client
                .prepareSearch()
                .setSearchType(SearchType.QUERY_THEN_FETCH)
                .setQuery(matchQueryBuilder3);


        MultiSearchResponse sr = client.prepareMultiSearch()
                .add(srb1)
                .add(srb2)
                .add(srb3)
                .get();



        List<SearchHit> searchHits = new ArrayList<>();

        for (MultiSearchResponse.Item item : sr.getResponses()) {
            SearchResponse response = item.getResponse();
            searchHits.addAll(Arrays.asList(response.getHits().getHits()));
        }

        List<Map<String,Object>> res = new ArrayList<>();

        for(SearchHit hit : searchHits){
            res.add(hit.getSourceAsMap());
        }
        return res.stream().distinct().collect(Collectors.toList());
    }
}
