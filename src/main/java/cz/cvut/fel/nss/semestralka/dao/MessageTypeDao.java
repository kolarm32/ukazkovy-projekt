package cz.cvut.fel.nss.semestralka.dao;

import cz.cvut.fel.nss.semestralka.model.Message;
import cz.cvut.fel.nss.semestralka.model.MessageType;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import java.util.List;

@Repository
public class MessageTypeDao extends Dao<MessageType> {

    public MessageTypeDao() {
        super(MessageType.class);
    }

    public List getAllMessages() {
        try{
            return em.createNamedQuery("MessageType.getMessages", MessageType.class)
                    .getResultList();
        }catch( NoResultException e){
            return null;
        }
    }
}
