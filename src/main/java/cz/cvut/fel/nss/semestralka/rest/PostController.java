package cz.cvut.fel.nss.semestralka.rest;

import cz.cvut.fel.nss.semestralka.model.Post;
import cz.cvut.fel.nss.semestralka.model.User;
import cz.cvut.fel.nss.semestralka.service.PostService;
import cz.cvut.fel.nss.semestralka.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

@RestController
@CrossOrigin(origins = "https://my-social-network-x.herokuapp.com/", allowCredentials="true", allowedHeaders = "*")
public class PostController {

    private PostService postService;
    private UserService userService;

    @Autowired
    public PostController(PostService postService,UserService userService) {
        this.postService = postService;
        this.userService = userService;
    }


    @GetMapping(value = "/posts",produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Post> getPosts(){
        return postService.findAll();
    }

    @GetMapping(value = "/user/{userId}/posts",produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Post> getPosts(@PathVariable Integer userId){
        User u = userService.find(userId);
        return postService.getMyPosts(u);
    }

    @PostMapping(value = "/user/{userId}/sharePost",produces = MediaType.APPLICATION_JSON_VALUE)
    public void addPost(@RequestBody Post post, @PathVariable Integer userId){
        User u = userService.find(userId);
        List<Post> timeline = u.getProfile().getTimeline();
        post.setOwner(u);
        post.setTimestamp(new Timestamp(System.currentTimeMillis()));
        timeline.add(post);
        userService.update(u);
        postService.persist(post);
    }

    @PutMapping(value = "/post",produces = MediaType.APPLICATION_JSON_VALUE)
    public void updatePost(@RequestBody Post post){
        postService.update(post);
    }

    @DeleteMapping(value = "/post/{id}",consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public void delete(@PathVariable Integer id){
        Post post = postService.find(id);
        postService.remove(post);
    }


}
