package cz.cvut.fel.nss.semestralka.service;

import cz.cvut.fel.nss.semestralka.dao.MessageDao;
import cz.cvut.fel.nss.semestralka.dao.MessageTypeDao;
import cz.cvut.fel.nss.semestralka.dao.ProfileDao;
import cz.cvut.fel.nss.semestralka.dao.UserDao;
import cz.cvut.fel.nss.semestralka.model.Message;
import cz.cvut.fel.nss.semestralka.model.MessageType;
import cz.cvut.fel.nss.semestralka.model.Role;
import cz.cvut.fel.nss.semestralka.model.User;
import cz.cvut.fel.nss.semestralka.repository.UserRepository;
import cz.cvut.fel.nss.semestralka.security.SecurityUtils;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.Client;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotBlank;
import java.io.IOException;
import java.util.List;
import java.util.Date;
import java.util.Objects;

import static org.elasticsearch.common.xcontent.XContentFactory.jsonBuilder;

@Service
public class UserService {

    private final UserDao userDao;
    private final PasswordEncoder passwordEncoder;
    private final ProfileDao profileDao;
    private final MessageTypeDao messageTypeDao;
    private final MessageDao messageDao;
    private final UserRepository userRepository;
    private Client client;


    @Autowired
    public UserService(Client client, UserDao userDao, PasswordEncoder passwordEncoder,ProfileDao profileDao,MessageTypeDao messageTypeDao,MessageDao messageDao,UserRepository userRepository) {
        this.userDao = userDao;
        this.client = client;
        this.userRepository = userRepository;
        this.messageDao = messageDao;
        this.messageTypeDao=  messageTypeDao;
        this.passwordEncoder = passwordEncoder;
        this.profileDao = profileDao;
    }

    @Transactional
    public void persist(User user) throws IOException {
        Objects.requireNonNull(user);
        userRepository.save(user);

//        IndexResponse response = client.prepareIndex("users", "users", String.valueOf(user.getId()))
//                .setType("users")
//                .setSource(jsonBuilder().startObject()
//                        .field("id",user.getId())
//                        .field("firstName", user.getFirstName())
//                        .field("lastName", user.getLastName())
//                        .field("email",user.getEmail())
//                        .endObject()
//                ).get();

        user.encodePassword(passwordEncoder);
        if(user.getRole() == null){
            user.setRole(Role.USER);
        }

        userDao.persist(user);
    }

    @Transactional
    public List<User> getFriends(User user){
        return userDao.getFriends(user);
    }

    @Transactional
    public User findUserByEmail(String email){
        return userRepository.findByEmail(email);
//        return userDao.findByEmail(email);
    }

    @Transactional
    public List<User> findByFirstName(String firstName){
        return userRepository.findByFirstName(firstName);
//        return userDao.findByEmail(email);
    }
    @Transactional
    public List<User> findByLastName(@NotBlank String lastName){
        return userRepository.findByLastName(lastName);
    }

    @Transactional
    public User find(Integer id) {
        return userDao.find(id);
    }

    @Transactional
    public List<User> getAllAccounts() {
        return userDao.getAllUsers();
    }

    @Transactional
    public User showCurrentUser() throws Exception {
        if (SecurityUtils.isAuthenticatedAnonymously()) System.out.println("Spatny pokus o prihlaseni");
        return userDao.find(SecurityUtils.getCurrentUser().getId());
    }

    @Transactional
    public User update(User user){
        return userDao.update(user);
    }

    @Transactional
    public User createUser(String email, String password, String firstName, String lastName, String birthDate, String gender) {
        return userDao.create(email, password, firstName, lastName, new Date(birthDate), User.Gender.valueOf(gender));
    }

    @Transactional
    public User createUser(User user) {
        user = userDao.create(user);
        return user;
    }

    @Transactional
    public void addFriend(User me, User friend){
        Objects.requireNonNull(me);
        Objects.requireNonNull(friend);


        MessageType ms = new MessageType(me);
        messageTypeDao.persist(ms);

        MessageType ms1 = new MessageType(friend);
        messageTypeDao.persist(ms1);


        me.getChat().put(friend.getEmail(),ms);
        me.addFriend(friend);

        friend.getChat().put(me.getEmail(),ms1);
        friend.addFriend(me);

//        messageTypeDao.update(ms);
//        messageTypeDao.update(ms1);
        userDao.update(me);
        userDao.update(friend);
    }

    @Transactional
    public void deleteFriend(User me, User noMoreFriend){
        Objects.requireNonNull(me);
        Objects.requireNonNull(noMoreFriend);

        me.deleteFriend(noMoreFriend.getEmail());
        userDao.update(me);

        noMoreFriend.deleteFriend(me.getEmail());
        userDao.update(noMoreFriend);
    }

    @Transactional
    public void sendMessage(User me, String email, Message message){
        Objects.requireNonNull(message);
        Objects.requireNonNull(email);
        Objects.requireNonNull(me);

        messageDao.persist(message);
        me.getChat().get(email).addMessage(message);
        messageTypeDao.update(me.getChat().get(email));

        System.out.println(me.getChat().get(email).getMessages());
        messageDao.update(message);
        userDao.update(me);

    }

    @Transactional
    public List getMessages(){
        return messageTypeDao.getAllMessages();
    }
}

