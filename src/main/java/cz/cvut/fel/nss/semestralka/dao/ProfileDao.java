package cz.cvut.fel.nss.semestralka.dao;


import cz.cvut.fel.nss.semestralka.model.Profile;
import cz.cvut.fel.nss.semestralka.model.User;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;

@Repository
public class ProfileDao extends Dao<Profile> {
    protected ProfileDao() {
        super(Profile.class);
    }

    public User findByUsername(String[] name) {
        try{
            return em.createNamedQuery("User.findByUsername", User.class)
                    .setParameter("firstName",name[0])
                    .setParameter("firstName",name[1])
                    .getSingleResult();
        }catch( NoResultException e){
            return null;
        }
    }

}
