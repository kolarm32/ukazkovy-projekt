package cz.cvut.fel.nss.semestralka.dao;


import cz.cvut.fel.nss.semestralka.model.Comment;
import cz.cvut.fel.nss.semestralka.model.Post;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import java.util.List;

@Repository
public class CommentDao extends Dao<Comment> {

    protected CommentDao() {
        super(Comment.class);
    }

    public List<Comment> getComments(Post post){
        try{
            return em.createNamedQuery("Comment.getComments", Comment.class).setParameter("postID",post.getId()).getResultList();
        }catch( NoResultException e){
            return null;
        }
    }
}
