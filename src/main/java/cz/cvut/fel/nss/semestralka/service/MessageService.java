package cz.cvut.fel.nss.semestralka.service;

import cz.cvut.fel.nss.semestralka.dao.MessageDao;
import cz.cvut.fel.nss.semestralka.model.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class MessageService {

    private MessageDao messageDao;

    @Autowired
    public MessageService(MessageDao messageDao) {
        this.messageDao = messageDao;
    }

    @Transactional
    public void addMessage(Message message){
        messageDao.persist(message);
    }

    @Transactional
    public void addMessages(List<Message> messages){
        messages.forEach(message -> messageDao.persist(message));
    }

    @Transactional
    public List getAllmessages(){
        return messageDao.getAllMessages();
    }

}
