package cz.cvut.fel.nss.semestralka.service;

import cz.cvut.fel.nss.semestralka.dao.UserDao;

import cz.cvut.fel.nss.semestralka.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetailsSecurityService implements org.springframework.security.core.userdetails.UserDetailsService{

    private final UserDao userDao;

    @Autowired
    public UserDetailsSecurityService(UserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        final User user = userDao.findByEmail(email);
        if (user == null) {
            throw new UsernameNotFoundException("User with email " + email + " not found.");
        }
        return new cz.cvut.fel.nss.semestralka.security.model.UserDetails(user);
    }
}
