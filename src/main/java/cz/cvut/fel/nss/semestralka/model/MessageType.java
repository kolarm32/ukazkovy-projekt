package cz.cvut.fel.nss.semestralka.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import java.util.ArrayList;
import java.util.List;

@Entity
@NamedQuery(name = "MessageType.getMessages", query = "SELECT m FROM MessageType m")
public class MessageType  extends AbstractEntity{

    @ManyToOne
    @JsonIgnore
    private User sender;

    @ManyToMany
    private List<Message> messages = new ArrayList<>();

    public MessageType() {
    }

    public User getSender() {
        return sender;
    }

    public MessageType(User sender) {
        this.sender = sender;
    }

    public void setSender(User sender) {
        this.sender = sender;
    }

    public List<Message> getMessages() {
        return messages;
    }

    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }

    public void addMessage(Message message){
        this.messages.add(message);
    }
}
