package cz.cvut.fel.nss.semestralka;

import cz.cvut.fel.nss.semestralka.model.Message;
import cz.cvut.fel.nss.semestralka.model.User;
import cz.cvut.fel.nss.semestralka.service.MessageService;
import cz.cvut.fel.nss.semestralka.service.PostService;
import cz.cvut.fel.nss.semestralka.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.*;

@Repository
public class MainAfter implements CommandLineRunner {

    @Autowired
    private UserService userService;
    @Autowired
    private PostService ps;
    @Autowired
    private MessageService messageService;

    @Override
    public void run(String... args) throws Exception {
        User account1 = new User(new Date(1-12-2000), "Matej", "Kulich", User.Gender.MALE, "matej@mysocialnetwork.com","admin");
        User account2 = new User(new Date(1-12-2000), "Michal", "Kolar", User.Gender.MALE, "michal@mysocialnetwork.com","admin");
        User guest = new User(new Date(1-12-2000), "guest", "guest", User.Gender.FEMALE, "guest@mysocialnetwork.com","guest");


        userService.persist(account1);
        userService.persist(account2);
        userService.persist(guest);

        userService.addFriend(account1,guest);
        userService.addFriend(account1,account2);
        userService.addFriend(guest,account2);

    }
}
